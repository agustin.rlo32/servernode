const express = require('express');
const products =[
    {
        id: 100,
        nombre: 'Producto 5'
    },
    {
        id: 101,
        nombre: 'Producto 6'
    }
]
const app = express();
const port = 3001;

app.get('/', (req, res) => {
    res.send('Hola mi server en express');
}) 
app.get('/nueva-ruta', (req, res) => {
    res.send('Hola, soy un nuevo point');
})

app.get('/products', (req, res) => {
    const products = [];
    for (let index = 0; index < 100; index++) {
        products.push({
            name: Faker.commerce.productName(),
            price: parseInt(Faker.commerce.price(), 10),
            image: Faker.image.imageUrl(),
        })
    }
    res.json(products);
});

app.get('/products/:id', (req, res) => {
    const { id } = req.params;
res.json({
    id,
    name:'Producto 2',
    price: 2000
});
})

app.get('/users', (req, res) => {
    const { limits, offset } = req.query;
    if (limits && offset) {
        res.json ({
            limits,
            offset
        });
    } else {
        res.send('No hay parametros');
    }
})

app.get('/categories/:categoryId/products/:productId', (req, res) => {
    const { categoryId, productId } = req.params;
    res.json({
        categoryId,
        productId,
    })
})

app.listen(port, () => {
    console.log('MI port' + port);
});